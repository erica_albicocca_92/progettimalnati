﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using Newtonsoft.Json;
using System.Xml;


namespace ClientProject
{



    public struct serverInfos
    {
        //public:
        public Socket s;
        public string name;
        //string focusPID;

        public serverInfos(Socket sock, string n)
        {
            s = sock;
            name = n;
            //focusPID = null;
        }

    };



    public partial class ClientProject : Form
    {
        public Dictionary<string, Socket> addrSocks = new Dictionary<string, Socket>();
        volatile string actual_server;


        ConnectionObject co;


        Dictionary<string, List<string>> dict_temp = new Dictionary<string, List<string>>();

        public delegate void UpdateItem(List<appInfos> lista);
        public UpdateItem updateDelegate;

        public delegate void refres();
        public refres refDel;
        public delegate void closeConnItem(object sender, EventArgs e);
        public closeConnItem closeConnDelegate;

        public delegate void chiudereDelegate();
        public chiudereDelegate chiudere;

        public delegate void notifyItem();
        public notifyItem notifyDelegate;

        public delegate void appComboItem(String name);
        //public appComboItem appComboDelegate;

        public Dictionary<string, int> focusTimes = new Dictionary<string, int>();
        public string focusPid = null;

        public bool isConnected = false;

        public static int totTimes= 0;
        public ClientProject()
        {
            InitializeComponent();
            updateDelegate = new UpdateItem(updateGrid);
            closeConnDelegate = new closeConnItem(closeConnButton_Click);
            chiudere = new chiudereDelegate(chiudi);
            //appComboDelegate = new appComboItem(updateAppComboBox);
            notifyDelegate = new notifyItem(notifyFocus);
            refDel = new refres(this.dataGridView1.Refresh); 
        }

        public void notifyFocus()
        {
           /* XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText);

            // Fill in the text elements
            XmlNodeList stringElements = toastXml.GetElementsByTagName("text");
            for (int i = 0; i < stringElements.Length; i++)
            {
                stringElements[i].AppendChild(toastXml.CreateTextNode("Line " + i));
            }

            // Specify the absolute path to an image
            String imagePath = "file:///" + Path.GetFullPath("toastImageAndText.png");
            XmlNodeList imageElements = toastXml.GetElementsByTagName("image");

            ToastNotification toast = new ToastNotification(toastXml);*/

        }

        public void chiudi()
        {
            // Console.WriteLine("dentro chiudi\n");
            //checkConnButton.Enabled = false;
            closeConnButton.Enabled = false;
            sendCharButton.Enabled = false;
            okConnButton.Enabled = true; 
        }

       /* private void updateAppComboBox(String name)
        {
            appComboBox.Text = name;
            appComboBox.Refresh();
            //appComboBox.Text=name;
        }
        */
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void sendCharButton_Click(object sender, EventArgs e)
        {
            if (( !modifierText.Text.Equals("") ))
            {
                Socket s = addrSocks[actual_server];
                //mandare anche la quantità dei byte
                byte[] msg = Encoding.UTF8.GetBytes(focusPid + ";" + modifierText.Text +"ENDMSG");
                /*int countBytes = msg.Count();//così?
                byte[] msg0 = Encoding.UTF8.GetBytes("SIZE:" + countBytes);
                s.Send(msg0, 7, SocketFlags.None);*/
                try
                {
                    s.Send(msg);
                   // Console.WriteLine("sent: " + modifierText.Text);
                }catch(SocketException exc)
                {
                    System.Windows.Forms.MessageBox.Show("Connection closed by the server because of client inactive!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //chiusura connessione e chiusura thread
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                    addrSocks.Remove(actual_server);
                    ClientProject.totTimes = 0;
                    chiudere();

                    //remoteClient.closeConnButton.Enabled = false;
                    //okConnButton.Enabled = true;
                    return ;
                }
                //inserire messaggio -> sent!
                System.Windows.Forms.MessageBox.Show("Message sent!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Send failed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            modifierText.Text = "";
            //charText.Text = "";

        }



       /* public static void ThreadProc()
        {
            co.performConnection();
            /*for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("ThreadProc: {0}", i);
                // Yield the rest of the time slice.
                Thread.Sleep(0);
            }
        }*/


        private void okConnButton_Click(object sender, EventArgs e)
        {

            Boolean okFlag = false;
            Char delimiter = '.';
            String[] substrings = null;
            
            try
            {
                substrings = serverAddrText.Text.Split(delimiter);
                //okFlag = true;
                //substrings = null;
                //Console.WriteLine("wrong format"); 
                
                if (substrings != null && substrings.Count() == 4)
                {
                    
                    Int16 a;
                    //try {  }
                    a = Convert.ToInt16(substrings[0]);
                    /*catch (Exception ex) when (ex is System.FormatException || ex is System.OverflowException )
                    {
                        a = -1;
                        Console.WriteLine("dentroa");
                    }
                   */
                    if (a < 256 && a >= 0)
                    {
                        
                        Int16 b = Convert.ToInt16(substrings[1]);
                        if (b < 256 && b >= 0)
                        {
                            
                            Int16 c = Convert.ToInt16(substrings[2]);
                            if (c < 256 && c >= 0)
                            {
                                
                                Int16 d = Convert.ToInt16(substrings[3]);
                                if (d < 256 && d >= 0)
                                {
                                    
                                    okFlag = true;
                                    //connect to address
                                   // Console.WriteLine("connected :)");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) when (ex is System.FormatException || ex is System.OverflowException) { Console.WriteLine("eccezione!"); }
            if (okFlag == false)
            {
                //wrong address
                //Console.WriteLine("wrong address!!");
                System.Windows.Forms.MessageBox.Show("Indirizzo errato, riprovare!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                //connection!
                Int16 port = -1;
                //Console.WriteLine(Convert.ToInt16("27015"));
               // Console.WriteLine(portText.Text);
                try
                {
                    port = Convert.ToInt16(portText.Text);
                }
                catch (Exception ex) when (ex is System.FormatException || ex is System.InvalidCastException || ex is System.OverflowException)
                {
                    //Console.WriteLine("errore porta");
                    System.Windows.Forms.MessageBox.Show("Numero di porta errato!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                if (port > -1)
                {
                    try
                    {
                        string serverName = serverAddrText.Text;
                        //Console.WriteLine("porta buona");

                        //creare thread


                        IPAddress ipAddr = IPAddress.Parse(serverName);
                        IPEndPoint remoteEP = new IPEndPoint(ipAddr, port);
                        ManualResetEvent connectDone = new ManualResetEvent(false);
                        Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                        serverInfos si = new serverInfos(client, serverName);


                        //client.BeginConnect(remoteEP, new AsyncCallback(SocketCallback), si);
                        /////////
                        client.Connect(remoteEP);//new Dictionary<string, Dictionary<string,appInfos>>();
                        client.ReceiveTimeout = 10000; //in millisecondi
                        
                        Thread.Sleep(200);
                        isConnected = true;
                       // checkConnButton.Enabled = true;
                       // refreshButton.Enabled = true;
                        //Handshake
                        

                                var server_apps = new List<appInfos>();
                                
                                //dict_appinfos.Add(serverName, server_apps);
                                co = new ConnectionObject(this, client, serverName, port);
                        Thread clientTh = new Thread(new ThreadStart(co.performConnection));
                        clientTh.Start();


                        //thread per aspettare notifiche


                        //Thread t = new Thread(new ThreadStart(ThreadProc));
                        //t.Start();
                        //co.performConnection();
                                actual_server = serverName;
                        
                        //socketListen(client, serverName, _infos[serverName]);
                        ////////
                        addrSocks.Add(serverName, client);
                                client = null;
                                //serverComboBox.Items.Add(serverName);
                                sendCharButton.Enabled = true;
                                //connectDone.WaitOne();
                                closeConnButton.Enabled = true;
                                okConnButton.Enabled = false;
                                
                            
                        
                    }
                    catch (SocketException exc)
                    {
                       // Console.WriteLine("Impossible to connect!");
                        System.Windows.Forms.MessageBox.Show("Impossibile connettersi!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        
                        //return;
                    }
                }
                //int port = 11000;

            }

        }

        

        public static Icon BytesToIcon(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                //Icon icona= new Icon(;
                //icona.Save(ms);
                return new Icon(ms);
            }
        }

        public static double percFocus(int times)
        {
            return ((double)(100 * times) /(double) totTimes) ;
        }

        private void updateGrid(List<appInfos> lista)
        {

            // totTimes++;
            Invoke((MethodInvoker)delegate
            {
                dataGridView1.Rows.Clear();
            });
            //dataGridView1.Rows.Clear();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                dataGridView1.Rows.Remove(row);
            }
            Invoke((MethodInvoker)delegate
            {
                dataGridView1.Refresh();
            });
            //dataGridView1.Invoke(this.refDel());
            //dataGridView1.Invoke(new Delegate(Refresh()));
            //dataGridView1.Refresh();
            foreach (appInfos apin in lista)
            {
                if (apin.icon != null)
                {
                    Icon gatto = BytesToIcon(apin.icon);
                    //dataGridView1.Rows.Add(apin.focus, gatto, apin.name, focusTimes[apin.pid]);
                    if (focusTimes.ContainsKey(apin.pid))
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            dataGridView1.Rows.Add(apin.focus, gatto, apin.name, String.Format("{0:0.00}", percFocus(focusTimes[apin.pid])));
                        });
                        
                    }
                    else
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            dataGridView1.Rows.Add(apin.focus, gatto, apin.name, 0.00);
                        });
                        
                    }
                }
                else { 
                    //mettere icona no icon?
                    //Icon nulla = Icon.
                    Icon nulla = Icon.ExtractAssociatedIcon("rClient.ico");
                    if (focusTimes.ContainsKey(apin.pid))
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            dataGridView1.Rows.Add(apin.focus, nulla, apin.name, String.Format("{0:0.00}", percFocus(focusTimes[apin.pid])));
                        });
                        
                    }
                    else
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            dataGridView1.Rows.Add(apin.focus, nulla, apin.name, 0.00);
                        });
                        
                    }
                }
            }
            //dataGridView1.Rows.Add(false, gatto, "due", "0");
            //dataGridView1.Rows.Add(true, img, "uno", "4");
            //dataGridView1.Refresh();
            Invoke((MethodInvoker)delegate
            {
                dataGridView1.Refresh();
            });
            //dataGridView1.Columns.

            Displaynotify();

            //Console.WriteLine("dentro updateGrid");
            //MessageBox.Show("updatatogriglia");
        }

        private void cancelConnButton_Click(object sender, EventArgs e)
        {
            serverAddrText.Clear();
            portText.Clear();
        }

        private void closeConnButton_Click(object sender, EventArgs e)
        {
            String server = actual_server;
            isConnected = false;
            //refreshButton.Enabled = false;
            if (addrSocks.ContainsKey(server))
            {
                //Console.WriteLine("dentro close button, server trovato");
                Socket s = addrSocks[actual_server];
                String toSend = "CLOSECONNECTION";
                byte[] bytes = new byte[15];
                bytes = System.Text.Encoding.UTF8.GetBytes(toSend);
                try
                {
                    s.Send(bytes);
                    //Socket s = addrSocks[server];
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                    s.Dispose();
                    
                } catch(SocketException exc)
                {
                    System.Windows.Forms.MessageBox.Show("Connessione con il server già terminata!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Console.WriteLine(exc.ErrorCode);
                }
                addrSocks.Remove(server);
                ClientProject.totTimes = 0;
                //dict_appinfos.Remove(server);
                //serverComboBox.Items.Remove(server);
                //serverComboBox.Text = "";
             //   Console.WriteLine("Connection with " + server + " closed.");

                if (addrSocks.Count == 0)
                {
                    sendCharButton.Enabled = false;
                }
                totTimes = 0;
                focusTimes.Clear();
                System.Windows.Forms.MessageBox.Show("Connessione chiusa!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                closeConnButton.Enabled = false;
                okConnButton.Enabled = true;
            }
        }

        public void Displaynotify()
        {
            try
            {
                RCnotify.BalloonTipTitle = "Client Project";
                RCnotify.BalloonTipText = "Qualcosa è cambiato!";
                RCnotify.ShowBalloonTip(100);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("problema notifica!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void serverAddrText_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void addModButton_Click(object sender, EventArgs e)
        {
            modifierText.Text += modComboBox.Text + ";";//modificatori duplicati? mandati tutti insieme o in sequenza?
        }

        private void clearModButton_Click(object sender, EventArgs e)
        {
            modifierText.Text = "";
        }

        private void serverComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //actual_server = serverComboBox.ValueMember;
          //  Console.WriteLine("actual_server= " + actual_server);
        }

        private void RCnotify_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        /*private void refreshButtonClick(object sender, EventArgs e)
        {
            if (isConnected)
            {
                //richiesta dati
                if (co.ask())
                {
                    //prende dati
                    Thread.Sleep(1150);
                    co.performConnection();
                }
                else
                {
                    isConnected = false;
                    refreshButton.Enabled = false;
                }
                    
                
            }
            
        }*/

        private void addCharButton_Click(object sender, EventArgs e)
        {
            modifierText.Text += charComboBox.Text + ";";
        }

        private void RemoteClient_Load(object sender, EventArgs e)
        {

        }

        private void checkConnButton_Click(object sender, EventArgs e)
        {
            Socket s = addrSocks[actual_server];
            String toSend = "HLO";
            byte[] bytes = new byte[3];
            bytes = System.Text.Encoding.UTF8.GetBytes(toSend);
            try
            {
                s.Send(bytes);
                //Socket s = addrSocks[server];
                //s.Shutdown(SocketShutdown.Both);
                //s.Close();
                //s.Dispose();

            }
            catch (SocketException exc)
            {
                System.Windows.Forms.MessageBox.Show("Server inattivo!\nChiusura connessione.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                chiudi();
               
                Console.WriteLine(exc.ErrorCode);

            }
        }
    }


    class ConnectionObject
    {
        Socket s;
        string serverName;
        Int16 port;
        List<appInfos> infos = new List<appInfos>();
        //string focusPid = null;
        ClientProject remoteClient;

        public ConnectionObject(ClientProject rc, Socket so, string name, Int16 p)
        {
            s = so;
            serverName = name;
            port = p;
            remoteClient = rc;
            remoteClient.focusPid = "";
            //infos = apps;
        }

        public bool ask()
        {
            //Byte[] buf = new byte[512];
            String toSend = "SENDDATA";
            byte[] bytes = new byte[8];
            bytes = System.Text.Encoding.UTF8.GetBytes(toSend);
            try
            {
                s.Send(bytes);
            }
            catch (SocketException exc)
            {
                //if (exc.SocketErrorCode == SocketError.TimedOut)
                //{
                    //Console.WriteLine("Connection closed by the server because of client inactive!");
                    System.Windows.Forms.MessageBox.Show("" + exc.ErrorCode, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //chiusura connessione e chiusura thread
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                    remoteClient.addrSocks.Remove(serverName);
                    ClientProject.totTimes = 0;
                remoteClient.chiudere();
                    
                    //remoteClient.closeConnButton.Enabled = false;
                    //okConnButton.Enabled = true;
                return false;
                    //remoteClient.closeConnDelegate(this, null);
               // }
            }
            return true;
        }

        public void performConnection()
        {

            //serverInfos
            //List<string> tempPIDs = new List<string>();//lista di pid
            //Boolean isChanged = false;//se bisogna fare update di grid

            //Thread sendTh = new Thread(new ThreadStart(this.send));


            //Console.WriteLine("I'm listening on address " + serverName);
            //String toSend = "SENDDATA";
            byte[] bytes = new byte[512];
            //bytes = System.Text.Encoding.UTF8.GetBytes(toSend);
            //Console.WriteLine("received: \n" + result);
            Thread.Sleep(350);
            //s.Blocking = false;

            Boolean flagUpdate = false;
            while (true)
            {
                //{
                s.ReceiveTimeout = Timeout.Infinite;
                s.SendTimeout = 5000;
                

                //string result = System.Text.Encoding.UTF8.GetString(byteArray);
                byte[] recvBytes = new byte[12];
                string result = "";
                while (true)
                {
                    

                    int n = 0;
                    if(remoteClient.isConnected== false)
                    {
                        return;
                    }
                    try
                    {
                        //mandare una cosa per capire se il server è ancora attivo
                        byte[] recvSnd = new byte[3];
                        //
                        n = s.Receive(recvSnd);
                        result = System.Text.Encoding.UTF8.GetString(recvSnd);
                        //Console.WriteLine("received: \n" + result);
                        if (!result.Equals("SND"))
                        {
                            continue;
                        }

                        break;
                    }

                    catch (SocketException exc)
                    {

                        if (exc.SocketErrorCode == SocketError.TimedOut)
                        {
                            //Console.WriteLine("Connection problem! Shut down!");
                            System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            ClientProject.totTimes = 0;
                            remoteClient.chiudere();
                            return;
                            //remoteClient.closeConnDelegate(this, null);
                        }
                        else if(exc.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            System.Windows.Forms.MessageBox.Show("Chiusura connessione da parte del server!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            ClientProject.totTimes = 0;
                            remoteClient.chiudere();
                            return;
                        }
                    }
                }
                Thread.Sleep(600);
                    while (true) {
                    if (remoteClient.isConnected == false)
                    {
                        
                        return;
                    }
                    try
                    {
                        int n = s.Receive(recvBytes);
                        result = System.Text.Encoding.UTF8.GetString(recvBytes);
                    }
                    catch (SocketException exc)
                    {

                        if (exc.SocketErrorCode == SocketError.TimedOut)
                        {
                            //Console.WriteLine("Connection problem! Shut down!");
                            System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            ClientProject.totTimes = 0;
                            remoteClient.chiudere();
                            return;
                            //remoteClient.closeConnDelegate(this, null);
                        }
                        else if(exc.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            System.Windows.Forms.MessageBox.Show("Chiusura connessione da parte del server!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            ClientProject.totTimes = 0;
                            remoteClient.chiudere();
                            return;
                        }
                    }

                    //Console.WriteLine("received: \n" + result);
                    /* if (!result.Equals("") && n>0)
                     {
                         var fff = result.Split(' ');
                         if (fff.Count() == 3)
                             break;
                     }*/
                    // }
                    //var numeroBytes = result.Substring(7, 5);
                    s.ReceiveTimeout = 20000;
                    var splitti = result.Split(' ');
                    if (splitti.Count() < 3)
                    {
                        //s.Shutdown(SocketShutdown.Both);
                        //s.Close();
                        System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //chiusura connessione e chiusura thread
                        s.Shutdown(SocketShutdown.Both);
                        s.Close();
                        remoteClient.addrSocks.Remove(serverName);
                        ClientProject.totTimes = 0;
                        remoteClient.chiudere();
                        return;
                        //remoteClient.closeConnDelegate(this, null);
                    }
                    var splitti2 = splitti[2].Split('\0');

                    int nb = Int32.Parse(splitti2[0]);

                    byte[] recvBytesJson = new byte[200];
                    Array.Resize(ref recvBytesJson, nb +1);
                    int bytesRicevuti = 0;
                    if (remoteClient.isConnected == false)
                    {
                        return;
                    }
                    try
                    {
                        bytesRicevuti=s.Receive(recvBytesJson);
                       
                        //Console.WriteLine("prendendo json ricevuti " + bytesRicevuti + " bytes\ndovevo riceverne " + nb + "\n");
                    }
                    catch (SocketException exc)
                    {
                        if (exc.SocketErrorCode == SocketError.TimedOut)
                        {
                            //Console.WriteLine("Connecticon problem! Shut down!");
                            System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            remoteClient.chiudere();
                            ClientProject.totTimes = 0;
                            
                            return;
                            //remoteClient.closeConnDelegate(this, null);
                        }
                        else if (exc.SocketErrorCode == SocketError.ConnectionReset)
                        {
                            System.Windows.Forms.MessageBox.Show("Chiusura connessione da parte del server!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //chiusura connessione e chiusura thread
                            s.Shutdown(SocketShutdown.Both);
                            s.Close();
                            remoteClient.addrSocks.Remove(serverName);
                            ClientProject.totTimes = 0;
                            remoteClient.chiudere();
                            return;
                        }
                    }

                    result = System.Text.Encoding.UTF8.GetString(recvBytesJson);
                   // Console.WriteLine("received: \n" + result);

                    JsonElement elemento;
                    Thread.Sleep(200);
                    try
                    {
                        //Console.WriteLine("json da deserializzare: " + result);
                        elemento = JsonConvert.DeserializeObject<JsonElement>(result);
                        
                    }
                    catch (JsonException je)
                    {
                        //Console.WriteLine("Connection problem! Shut down!");
                        System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //chiusura connessione e chiusura thread
                        s.Shutdown(SocketShutdown.Both);
                        s.Close();
                        remoteClient.addrSocks.Remove(serverName);
                        remoteClient.chiudere();
                        ClientProject.totTimes = 0;
                        return;
                    }

                    /*try
                    {
                        Console.WriteLine(elemento.type);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Connection problem! Shut down!");
                        System.Windows.Forms.MessageBox.Show("Provlema connessione, elemento null pointer, dovevo prendere "+nb+"ma ho preso "+bytesRicevuti+"\n" + result, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //chiusura connessione e chiusura thread
                        s.Shutdown(SocketShutdown.Both);
                        s.Close();
                        remoteClient.addrSocks.Remove(serverName);
                        remoteClient.chiudere();
                        RemoteClient.totTimes = 0;
                        return;
                    }*/
                        if (elemento.type.Equals("1"))//true, window   //eccezione null pointer
                    {

                        appInfos ai;
                        if (!elemento.hasIcon.Equals("true"))
                        {
                            ai = new appInfos(elemento.name, elemento.pid, false, null, false);
                        }
                        else
                        {

                            ai = new appInfos(elemento.name, elemento.pid, false, null, true);
                            //ricevere icona
                            byte[] iconnn = new byte[2000];
                            Array.Resize(ref iconnn, Int32.Parse(elemento.size));
                            //for (int hh = 0; hh < elemento.size; hh++)
                            //{
                            if (remoteClient.isConnected == false)
                            {
                                return;
                            }
                            try
                            {
                                //s.Receive(iconnn);
                                s.Receive(iconnn, Int32.Parse(elemento.size), SocketFlags.None);
                            }
                            catch (SocketException exc)
                            {
                                if (exc.SocketErrorCode == SocketError.TimedOut)
                                {
                                    //Console.WriteLine("Connection problem! Shut down!");
                                    System.Windows.Forms.MessageBox.Show("Problema di connessione!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    //chiusura connessione e chiusura thread
                                    s.Shutdown(SocketShutdown.Both);
                                    s.Close();
                                    remoteClient.addrSocks.Remove(serverName);
                                    remoteClient.chiudere();
                                    ClientProject.totTimes = 0;
                                    //return;
                                    //remoteClient.closeConnDelegate(this, null);
                                }
                                else if (exc.SocketErrorCode == SocketError.ConnectionReset)
                                {
                                    System.Windows.Forms.MessageBox.Show("Chiusura connessione da parte del server!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    //chiusura connessione e chiusura thread
                                    s.Shutdown(SocketShutdown.Both);
                                    s.Close();
                                    remoteClient.addrSocks.Remove(serverName);
                                    ClientProject.totTimes = 0;
                                    remoteClient.chiudere();
                                    return;
                                }
                            }
                            //}
                            ai.icon = iconnn;
                        }
                        //ai.times_focus = remoteClient.focusTimes[ai.pid];
                        infos.Add(ai);
                        flagUpdate = true;
                    }
                    //prima si manda type = 3, con la lista dei pid e del loro focus times. se il pid è in infos, lo si updata del time
                    else if (elemento.type.Equals("2"))//false, focus
                    {
                        if (!remoteClient.focusPid.Equals(elemento.pid))
                        {
                            //notifyfocus delegate
                            remoteClient.notifyDelegate();
                        }

                        remoteClient.focusPid = elemento.pid;
                        //remoteClient.focusTimes[ remoteClient.focusPid]++;
                        foreach (appInfos ainfo in infos)
                        {
                            if (ainfo.pid.Equals(remoteClient.focusPid))
                            {
                                ainfo.setFocus(true);
                                //ainfo.incTimes();

                                // break;
                            }
                            else
                            {
                                ainfo.setFocus(false);
                            }

                        }
                        //remoteClient.appComboDelegate(elemento.name);
                        flagUpdate = true;
                    }
                    else if (elemento.type.Equals("3"))
                    {
                        var dict = elemento.map;
                        foreach (String k in dict.Keys)
                        {
                            if (k.Equals("end"))
                                continue;
                            remoteClient.focusTimes[k] = dict[k];
                            ClientProject.totTimes = Int32.Parse(elemento.size);

                        }
                    }
                    else if (elemento.type.Equals("0"))
                    {

                        remoteClient.updateDelegate(infos);
                        infos.Clear();
                        break;
                        //Thread.Sleep(5000);
                    }

                }
                
            }
           
        }

        private void send()
        {
            //mandare sequenze di tasti
            //throw new NotImplementedException();
        }

        public static Icon BytesToIcon(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                return new Icon(ms);
            }
        }

    }

    public class appInfos
    {

        public string name, pid;
        public Boolean focus;
        //public string icon;// momentaneamente
        public byte[] icon;
        public int times_focus;
        public Boolean hasFocus;

        public appInfos(string n, string p, Boolean f, byte[] i, Boolean hasFocus)
        {
            name = n;
            pid = p;
            focus = f;
            icon = i;
            times_focus = 0;
            this.hasFocus = hasFocus;
        }
        public void setFocus(bool value)
        {
            //get { return focus; }
            //set { focus = value; }
            focus = value;
        }
        public void incTimes()
        {
            times_focus++;
        }
    };


}