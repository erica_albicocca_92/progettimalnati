#pragma once
#include <json.hpp>
#include <Windows.h>
#include <map>

using json = nlohmann::json;

using namespace std;

class JsonWindow
{
private:
	std::map<string, string> window;
public:
	JsonWindow(int type, string name, string pid, bool hasIcon, char* icon, int size);
	JsonWindow(int type, map<string, int> mappa, int totTimes);
	void print();
	~JsonWindow();
	string saveToString();
	std::string getName();
	//void setIcon(string icon);
};

