#pragma once
#include <json.hpp>
#include <iostream>


using json = nlohmann::json;
using namespace std;

class JsonFocus
{
private:
	json focus;
public:
	JsonFocus(std::string pid);
	~JsonFocus();
	void print();
	void saveToFile();
};

